import math

a = float(input('Введите а:'))
x = float(input('Введите x:'))
g1 = 8 * (7 * a ** 2 + 61 * a * x + 40 * x ** 2)  # числитель функции G
g2 = 18 * a ** 2 - 11 * a * x + x ** 2  # знаменатель функции G
G = g1 / g2
print('Функция G:', G)

d = math.sin(27 * a ** 2 + 12 * a * x - 20 * x ** 2 - math.pi / 2)  # знаменатель функции F
F = (-1) / d
print('Функция F:', F)

Y = math.asin(45 * a ** 2 + 46 * a * x + 8 * x ** 2)
print('Функция Y:', Y)

