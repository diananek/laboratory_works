import math


rnd = True
result_dict = {'G': [], 'F': [], 'Y': []}

while rnd:

    try:
        a = float(input('Введите а:'))
        x_min = float(input('Введите минимальное значение x:'))
        x_max = float(input('Введите максимальное значение x:'))
        n = int(input('Введите количество шагов вычисления функции:'))
    except ValueError:
        print('Ошибка ввода')
        exit(1)

    x_lst = []  # Список x

# Вычисление шага изменения аргумента x

    step = (x_max - x_min) / n
    file = open('results.txt', 'w')

# Вычисление функции G

    for i in range(n):
        x = x_min + step * i
        x_lst.append(x)
        g1 = 8 * (7 * a ** 2 + 61 * a * x + 40 * x ** 2)  # числитель функции G
        g2 = 18 * a ** 2 - 11 * a * x + x ** 2  # знаменатель функции G
        if math.isclose(g2, 0, abs_tol=0.0001):
            G = None
            result_dict['G'].append(G)
        else:
            G = g1 / g2
            result_dict['G'].append(G)

    # Вычисление функции F

    for i in range(n):
        x = x_min + step * i
        d = math.sin(27 * a ** 2 + 12 * a * x - 20 * x ** 2 - math.pi / 2)  # знаменатель функции F
        if math.isclose(d, 0, abs_tol=0.0001):
            F = None
            result_dict['F'].append(F)
        else:
            F = (-1) / d
            result_dict['F'].append(F)

    # Вычисление функции Y

    for i in range(n):
        x = x_min + step * i
        m = 45 * a ** 2 + 46 * a * x + 8 * x ** 2
        if -1 <= m <= 1:
            Y = math.asin(m)
            result_dict['Y'].append(Y)
        else:
            Y = None
            result_dict['Y'].append(Y)

    # Вывод значений аргумента и функций в файл

    file.write(f'G:{",".join(["""Нет решения""" if y is None else str(f"""{y:.3f}""") for y in result_dict["G"]])}\n')
    file.write(f'F:{",".join(["""Нет решения""" if y is None else str(f"""{y:.3f}""") for y in result_dict["F"]])}\n')
    file.write(f'Y:{",".join(["""Нет решения""" if y is None else str(f"""{y:.3f}""") for y in result_dict["Y"]])}\n')
    file.close()

    result_dict["G"] = []
    result_dict["F"] = []
    result_dict["Y"] = []

    # Вывод

    file = open('results.txt', 'r')
    print(f'x: {", ".join([str(f"""{x:.3f}""") for x in x_lst])}\n')
    print(file.readline())
    print(file.readline())
    print(file.readline())
    file.close()

    # Продолжение или завершение цикла

    print('Если хотите продолжить нажмите 1, иначе - нажмите любую другую цифру')

    if int(input()) != 1:
        rnd = False

    else:
        x_lst = []



