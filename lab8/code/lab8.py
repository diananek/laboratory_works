import random
import counter

try:
    amount_points = int(input('Введите количество точек:'))
    search_radius = int(input('Введите радиус поиска:'))
except ValueError:
    print('Ошибка ввода')

points_list = [(random.randint(-50, 50), random.randint(-50, 50)) for i in range(amount_points)]
point_center = (random.randint(-50, 50), random.randint(-50, 50))

print(f'Количество точек в заданном радиусе:{counter.point_search(points_list, point_center, search_radius)}')






