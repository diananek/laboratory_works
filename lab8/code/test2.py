import time
import counter
import random
import matplotlib.pyplot as plt

time_lst = []
n_lst = []

for n in range(int(10e4), int(10e5), int(10e4)):
    time_temp = 0
    points_list = [(random.randint(-50, 50), random.randint(-50, 50)) for point in range(n)]
    point_center = (random.randint(-50, 50), random.randint(-50, 50))
    radius = random.randint(-50, 50)
    for _ in range(3):
        start = time.time()
        count = counter.point_search(points_list, point_center, radius)
        end = time.time()
        time_temp += end - start
    time_lst.append(time_temp / 3)
    print(time_lst[-1])
    n_lst.append(n)
    file = open("test_results.txt", 'a')
    file.write(f'{time_lst[-1]:.5f}\n')


plt.plot(n_lst, time_lst, 'o')
plt.plot(n_lst, time_lst, 'b')
plt.xlabel('кол-во точек')
plt.ylabel('время выполнения, с.')
plt.show()
