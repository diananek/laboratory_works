import math
import matplotlib.pyplot as plt


rnd = True
result_dict = {'G': [], 'F': [], 'Y': []}

while rnd:

    try:
        a = float(input('Введите а:'))
        x_min = float(input('Введите минимальное значение x:'))
        x_max = float(input('Введите максимальное значение x:'))
        n = int(input('Введите количество шагов вычисления функции:'))
    except ValueError:
        print('Ошибка ввода')
        exit(1)

    x_lst = []  # Список x

# Вычисление шага изменения аргумента x

    step = (x_max - x_min) / n

# Вычисление функции G

    for i in range(n):
        x = x_min + step * i
        x_lst.append(x)
        g1 = 8 * (7 * a ** 2 + 61 * a * x + 40 * x ** 2)  # числитель функции G
        g2 = 18 * a ** 2 - 11 * a * x + x ** 2  # знаменатель функции G
        if math.isclose(g2, 0, abs_tol=0.0001):
            G = None
            result_dict['G'].append(G)
        else:
            G = g1 / g2
            result_dict['G'].append(G)

    # Построение графика
    plt.title("График функции G")  # заголовок
    plt.xlabel("x")  # ось абсцисс
    plt.ylabel("G")  # ось ординат
    plt.grid(which='major', linewidth=1.2)  # включение отображение сетки
    plt.plot(x_lst, result_dict['G'], c='red', label='G')  # построение графика
    plt.legend()
    plt.show()


    # Вычисление функции F

    for i in range(n):
        x = x_min + step * i
        d = math.sin(27 * a ** 2 + 12 * a * x - 20 * x ** 2 - math.pi / 2)  # знаменатель функции F
        if math.isclose(d, 0, abs_tol=0.0001):
            F = None
            result_dict['F'].append(F)
        else:
            F = (-1) / d
            result_dict['F'].append(F)

        # Построение графика
    plt.title("График функции F")  # заголовок
    plt.xlabel("x")  # ось абсцисс
    plt.ylabel("F")  # ось ординат
    plt.grid(linewidth=1.2)  # включение отображение сетки
    plt.plot(x_lst, result_dict['F'], c='red', label='F')  # построение графика
    plt.legend()
    plt.show()

    # Вычисление функции Y

    for i in range(n):
        x = x_min + step * i
        m = 45 * a ** 2 + 46 * a * x + 8 * x ** 2
        if -1 <= m <= 1:
            Y = math.asin(m)
            result_dict['Y'].append(Y)
        else:
            Y = None
            result_dict['Y'].append(Y)

    # Построение графика
    plt.title("График функции Y")  # заголовок
    plt.xlabel("x")  # ось абсцисс
    plt.ylabel("Y")  # ось ординат
    plt.grid(which='major', linewidth=1.2)  # включение отображение сетки
    plt.plot(x_lst, result_dict['Y'], 'r-', label='Y')  # построение графика
    plt.legend()
    plt.show()

    # Вывод значений аргумента и функций

    print(f'x: {", ".join([str(x) for x in x_lst])}')
    print(f'G: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.3f}""") for y in result_dict["G"]])}')
    print(f'F: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.3f}""") for y in result_dict["F"]])}')
    print(f'Y: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.5f}""") for y in result_dict["Y"]])}')

    # Продолжение или завершение цикла

    print('Если хотите продолжить нажмите 1, иначе - нажмите любую другую цифру')

    if int(input()) != 1:
        rnd = False
    else:
        x_lst = []
        result_dict["G"] = []
        result_dict["F"] = []
        result_dict["Y"] = []

