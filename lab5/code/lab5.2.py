try:
    number = int(input('Введите целое число:'))
except:
    print('0')
    exit(1)

count = 0

if number == 0:
    count = 1

else:
    while number != 0:
        if (number % 10) % 2 == 0:
            count += 1
        number //= 10

print(count)