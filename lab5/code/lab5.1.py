import math
import matplotlib.pyplot as plt


def coincidence(y):

    result = ' '.join(["""Нет решения""" if e is None else str(f"""{e:.0f}""") for e in y])
    return result.count(line_template)


def max_y(y):
    return sorted(list(filter(None, y)))[-1]


def min_y(y):
    return sorted(list(filter(None, y)))[0]

rnd = True

while rnd:

    try:
        a = float(input('Введите а:'))
        x_min = float(input('Введите минимальное значение x:'))
        x_max = float(input('Введите максимальное значение x:'))
        fcn_num = int(input('Введите 1, чтобы вычислить функцию G, 2 - функцию F, 3 - функцию Y:'))
        n = int(input('Введите количество шагов вычисления функции:'))
        line_template = (input('Введите шаблон результата функции:'))
    except ValueError:
        print('Ошибка ввода')
        exit(1)

    x_lst = []  # Список x
    y_lst = []  # Список y

# Вычисление шага изменения аргумента x

    step = (x_max - x_min) / n

# Вычисление функции G

    if fcn_num == 1:
        for n in range(n):
            x = x_min + step * n
            x_lst.append(x)
            g1 = 8 * (7 * a ** 2 + 61 * a * x + 40 * x ** 2)  # числитель функции G
            g2 = 18 * a ** 2 - 11 * a * x + x ** 2  # знаменатель функции G
            if math.isclose(g2, 0, abs_tol=0.0001):
                G = None
                y_lst.append(G)
            else:
                G = g1 / g2
                y_lst.append(G)

        # Построение графика

        plt.title("График функции G")  # заголовок
        plt.xlabel("x")  # ось абсцисс
        plt.ylabel("G")  # ось ординат
        plt.grid(which='major', linewidth=1.2)  # включение отображение сетки
        plt.plot(x_lst, y_lst, c='red', label='G')  # построение графика
        plt.legend()
        plt.show()

        # Вывод значений аргумента и функции

        print(f'x: {", ".join([str(x) for x in x_lst])}')
        print(f'G: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.3f}""") for y in y_lst])}')

        print('\n')

        # Вывод минимального и максимального значения функции

        print(f'Наименьшее значение функции: {min_y(y_lst):.3f}')
        print(f'Наибольшее значение функции: {max_y(y_lst):.3f}')

        # Поиск количества совпадений результата с шаблоном

        print(f'Количество совпадений шаблона с результатом:{coincidence(y_lst)}')

    # Вычисление функции F

    elif fcn_num == 2:
        for n in range(n):
            x = x_min + step * n
            x_lst.append(x)
            d = math.sin(27 * a ** 2 + 12 * a * x - 20 * x ** 2 - math.pi / 2)  # знаменатель функции F
            if math.isclose(d, 0, abs_tol=0.0001):
                F = None
                y_lst.append(F)
            else:
                F = (-1) / d
                y_lst.append(F)

        # Построение графика
        plt.title("График функции F")  # заголовок
        plt.xlabel("x")  # ось абсцисс
        plt.ylabel("F")  # ось ординат
        plt.grid(linewidth=1.2)  # включение отображение сетки
        plt.plot(x_lst, y_lst, c='red', label='F')  # построение графика
        plt.legend()
        plt.show()

        # Вывод значений аргумента и функции

        print(f'x: {", ".join([str(x) for x in x_lst])}')
        print(f'F: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.3f}""") for y in y_lst])}')

        print('\n')

        # Вывод минимального и максимального значения функции

        print(f'Наименьшее значение функции: {min_y(y_lst):.3f}')
        print(f'Наибольшее значение функции: {max_y(y_lst):.3f}')

        # Поиск количества совпадений результата с шаблоном

        print(f'Количество совпадений шаблона с результатом:{coincidence(y_lst)}')

    # Вычисление функции Y

    elif fcn_num == 3:
        for n in range(n):
            x = x_min + step * n
            x_lst.append(x)
            m = 45 * a ** 2 + 46 * a * x + 8 * x ** 2
            if -1 <= m <= 1:
                Y = math.asin(m)
                y_lst.append(Y)
            else:
                Y = None
                y_lst.append(Y)

        # Построение графика
        plt.title("График функции Y")  # заголовок
        plt.xlabel("x")  # ось абсцисс
        plt.ylabel("Y")  # ось ординат
        plt.grid(which='major', linewidth=1.2)  # включение отображение сетки
        plt.plot(x_lst, y_lst, 'r-', label='Y')  # построение графика
        plt.legend()
        plt.show()

        # Вывод значений аргумента и функции

        print(f'x: {", ".join([str(x) for x in x_lst])}')
        print(f'Y: {", ".join(["""Нет решения""" if y is None else str(f"""{y:.5f}""") for y in y_lst])}')

        print('\n')

        # Вывод минимального и максимального значения функции

        print(f'Наименьшее значение функции: {min_y(y_lst):.5f}')
        print(f'Наибольшее значение функции: {max_y(y_lst):.5f}')

        # Поиск количества совпадений результата с шаблоном

        print(f'Количество совпадений шаблона с результатом:{coincidence(y_lst)}')

    else:
        print('Ошибка ввода')


# Продолжение или завершение цикла

    print('Если хотите продолжить нажмите 1, иначе - нажмите любую другую цифру')

    if int(input()) != 1:
        rnd = False
